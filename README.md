# OneBox

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.0.
Node version used 14.15.4.

## Run locally

Run `npm i` to install all dependencies.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project.
