import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { CartService } from '../../services/cart.service';
import { EventsService } from '../../services/events.service';
import { Constants } from '../../utils/constants';
import { SessionModel } from '../../models/session.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy {
  @Input() currentEventId = null;
  private cartSubscription: Subscription;
  public constants = Constants;
  public cartData: Map<number, SessionModel[]>;

  constructor( private cartService: CartService, private eventsService: EventsService ) { }

  ngOnInit(): void {
    this.cartSubscription = this.cartService.getCartObservable().subscribe( ( cart: Map<number, SessionModel[]> ) => {
      this.cartData = cart;
    });
  }

  ngOnDestroy(): void {
    if ( this.cartSubscription ) {
      this.cartSubscription.unsubscribe();
    }
  }

}
