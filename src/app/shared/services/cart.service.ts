import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { SessionModel } from '../models/session.model';

@Injectable()
export class CartService {
  private cartSubject: ReplaySubject<Map<number, SessionModel[]>> = new ReplaySubject<Map<number, SessionModel[]>>();
  private cartData = new Map<number, SessionModel[]>();

  public getCartObservable(): Observable<Map<number, SessionModel[]>> {
    return this.cartSubject.asObservable();
  }

  public getCartData(): any {
    return this.cartData;
  }

  public getSessionData( eventId: number, date: string ): SessionModel | null {
    if ( this.cartData.has( eventId ) ) {
      return this.cartData.get( eventId ).find( session => session.date === date );
    }
    return null;
  }

  public updateCartSession( eventId: number, sessionData: SessionModel ): void {
    const sessions: SessionModel[] = this.cartData.get( eventId );
    sessions.map( ( session: SessionModel ) => session.date === sessionData.date ? sessionData : session );
    this.cartData.set( eventId, sessions );
    this.cartSubject.next( this.cartData );
  }

  public getTotalPurchased( eventId: number ): number {
    let totalPurchased = 0;
    if ( this.cartData.has( eventId ) ) {
      this.cartData.get( eventId ).forEach( ( session: SessionModel ) => {
        totalPurchased = totalPurchased + session.purchased;
      });
    }
    return totalPurchased;
  }

  public removeSession( eventId: number, reference: string ): void {
    const sessionData: SessionModel = this.getSessionData( eventId, reference );
    sessionData.purchased--;
    this.updateCartSession( eventId, sessionData );
  }
}
