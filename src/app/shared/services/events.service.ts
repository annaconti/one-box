import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventModel } from '../models/event.model';
import { plainToClass } from 'class-transformer';
import { Observable, ReplaySubject } from 'rxjs';
import { SessionModel } from '../models/session.model';

@Injectable()
export class EventsService {
  private eventsPath = 'assets/data/events.json';
  private eventInfoPath = 'assets/data/event-info-';
  private events: EventModel[] = [];
  private eventsListSubject: ReplaySubject<EventModel[]> = new ReplaySubject<EventModel[]>();

  constructor( private httpClient: HttpClient ) {
  }

  private async fetchJSONData( path: string ): Promise<any> {
    return await this.httpClient.get( path ).toPromise();
  }

  public getEventsListObservable(): Observable<EventModel[]> {
    return this.eventsListSubject.asObservable();
  }

  public async fetchEvents(): Promise<EventModel[]> {
    try {
      this.events = await this.fetchJSONData( this.eventsPath );
      if ( this.events && this.events.length ) {
        this.events = this.events.map( event => plainToClass( EventModel, event ) );
        this.events = this.events.sort( (a, b) => ( +a.endDate - +b.endDate ) );
        this.eventsListSubject.next( this.events );
        return this.events;
      } else {
        return [];
      }
    } catch ( e ) {
      console.error( e );
    }
  }

  public getEvents(): EventModel[] {
    return this.events;
  }

  public getEventNameById( id: number ): string {
    const events = this.getEvents();
    const eventData = events.find( event => event.id === id );
    return eventData && eventData.title ? eventData.title : '';
  }

  public async getEventInfo( id: string ): Promise<any> {
    try {
      const eventInfo = await this.fetchJSONData( this.eventInfoPath + id + '.json' );
      eventInfo.sessions.map( session => plainToClass( SessionModel, session ) );
      eventInfo.sessions.sort( (a, b) => ( +a.date - +b.date ) );
      return eventInfo;
    } catch ( e ) {
      console.error( e );
    }
  }
}
