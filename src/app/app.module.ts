import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './dashboard/header/header.component';
import { EventsGridComponent } from './dashboard/events-grid/events-grid.component';
import { EventCardComponent } from './dashboard/events-grid/event-card/event-card.component';
import { EventsService } from './shared/services/events.service';
import { HttpClientModule } from '@angular/common/http';
import { EventDetailComponent } from './dashboard/event-detail/event-detail.component';
import { CartService } from './shared/services/cart.service';
import { CartComponent } from './shared/components/cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    EventsGridComponent,
    EventCardComponent,
    EventDetailComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    EventsService,
    CartService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
