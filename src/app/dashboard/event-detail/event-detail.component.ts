import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../../shared/services/events.service';
import { Constants } from '../../shared/utils/constants';
import { CartService } from '../../shared/services/cart.service';
import { SessionModel } from '../../shared/models/session.model';
import { plainToClass } from 'class-transformer';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {
  public constants = Constants;
  public eventInfo: any;
  public cartData: Map<number, SessionModel[]>;


  constructor( private route: ActivatedRoute,
               private router: Router,
               private eventsService: EventsService,
               private cartService: CartService ) {
  }

  async ngOnInit(): Promise<void> {
    const eventId = this.route.snapshot.paramMap.get('id');
    this.eventInfo = await this.eventsService.getEventInfo( eventId );
    if ( this.eventInfo ) {
      this.initCounters();
    }
  }

  private initCounters(): void {
    this.cartData = this.cartService.getCartData();
    if ( this.cartData && !this.cartData.has( this.eventInfo.event.id ) ) {
      const sessions: SessionModel[] = this.eventInfo.sessions.map( session => plainToClass( SessionModel, session ) );
      this.cartData.set( this.eventInfo.event.id, sessions);
    }
  }

  public increaseCount( reference: string, max: number ): void {
    const sessionData: SessionModel = this.cartService.getSessionData( this.eventInfo.event.id, reference );
    sessionData.purchased = sessionData.purchased < max ? sessionData.purchased + 1 : sessionData.purchased;
    this.cartService.updateCartSession( this.eventInfo.event.id, sessionData );
  }

  public decreaseCount( reference: string ): void {
    const sessionData: SessionModel = this.cartService.getSessionData( this.eventInfo.event.id, reference );
    sessionData.purchased = sessionData.purchased >= 1 ? sessionData.purchased - 1 : sessionData.purchased;
    this.cartService.updateCartSession( this.eventInfo.event.id, sessionData );
  }
}
