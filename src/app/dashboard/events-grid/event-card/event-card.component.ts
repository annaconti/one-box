import { Component, Input, OnInit } from '@angular/core';
import { EventModel } from '../../../shared/models/event.model';
import { Constants } from '../../../shared/utils/constants';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent implements OnInit {
  @Input() eventData: EventModel;
  public constants = Constants;

  constructor() { }

  ngOnInit(): void {
  }

}
