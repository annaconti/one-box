import { Component, OnDestroy, OnInit } from '@angular/core';
import { EventModel } from '../../shared/models/event.model';
import { EventsService } from '../../shared/services/events.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-events-grid',
  templateUrl: './events-grid.component.html',
  styleUrls: ['./events-grid.component.scss']
})
export class EventsGridComponent implements OnInit, OnDestroy {
  private eventsListSubscription: Subscription;
  public eventsList: EventModel[];

  constructor( private eventsService: EventsService ) { }

  async ngOnInit(): Promise<void> {
    this.eventsListSubscription = this.eventsService.getEventsListObservable().subscribe( data => {
      this.eventsList = data;
    } );
  }

  ngOnDestroy(): void {
    if ( this.eventsListSubscription ) {
      this.eventsListSubscription.unsubscribe();
    }
  }

}
