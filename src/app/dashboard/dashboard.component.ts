import { Component, OnInit } from '@angular/core';
import { EventsService } from '../shared/services/events.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor( private eventsService: EventsService ) { }

  async ngOnInit(): Promise<any> {
    await this.eventsService.fetchEvents();
  }

}
