import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsGridComponent } from './dashboard/events-grid/events-grid.component';
import { EventDetailComponent } from './dashboard/event-detail/event-detail.component';

const routes: Routes = [
  { path: 'event-detail/:id', component: EventDetailComponent },
  { path: '**', component: EventsGridComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

